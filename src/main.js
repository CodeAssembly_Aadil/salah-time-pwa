// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import * as VueGoogleMaps from 'vue2-google-maps';

import App from 'App';
import LocalForage from 'localforage';
import Router from './router';
import Store from 'data/Store';
import Vue from 'vue';

const EventBus = new Vue()

Object.defineProperties(Vue.prototype, {
	$bus: {
		get: function () {
			return EventBus
		}
	}
})

LocalForage.config({
	// driver      : localforage.WEBSQL, // Force WebSQL; same as using setDriver()
	name: 'SalahTime',
	version: 1.0,
	size: 4980736, // Size of database, in bytes. WebSQL-only for now.
	storeName: 'salah_time', // Should be alphanumeric, with underscores.
	description: 'Offline data store for Salah Time app'
});

Vue.use(VueGoogleMaps, {
	load: {
		key: 'AIzaSyCFNb7a7jPk3L8Lq4QBQL1cCeyuQiU2PNA',
		// v: 'OPTIONAL VERSION NUMBER',
		libraries: 'places' // If you need to use place input
	}
});

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
	el: '#app',
	router: Router,
	store: Store,
	template: '<App/>',
	components: { App }
});
