export const QUEUE_TOAST = 'QUEUE_TOAST';
export const TOAST_SUCCSESS = 'success';
export const TOAST_ERROR = 'error';

export default function ToastEvent (message = 'toast', type = 'default', duration = 2750, next = false) {

	var event = Object.create(null);

	event.message = message;
	event.type = type;
	event.duration = duration;
	event.next = next;

	Object.freeze(event);

	return event;
}
