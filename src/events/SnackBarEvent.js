export const QUEUE_SNACKBAR = 'QUEUE_SNACKBAR';
export const SNACKBAR_ACTION = 'SNACKBAR_ACTION';
export const SNACKBAR_SUCCSESS = 'success';
export const SNACKBAR_ERROR = 'error';

export function SnackBarActionEvent(id, action) {

	var event = Object.create(null);

	event.id = id;
	event.action = action;

	Object.freeze(event);

	return event;
}

export default function SnackBarEvent(id, message = 'snackbar', action = 'OK', type = 'default', duration = 3750, forceInteraction = false, next = false) {

	var event = Object.create(null);

	event.id = id;
	event.next = next;

	event.message = message;
	event.action = action;
	event.type = type;
	event.duration = duration;
	event.forceInteraction = forceInteraction;
	
	Object.freeze(event);

	return event;
}
