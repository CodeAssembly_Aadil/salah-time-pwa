export const ROOT = '//api.salahtime.develop';

export const POST_REGISTER = ROOT + '/user/register';
export const GET_USER = ROOT + '/user';
export const PATCH_USER = ROOT + '/user';
export const DELETE_USER = ROOT + '/user';

export const POST_REQUEST_OTP = ROOT + '/request-otp';
export const POST_VALIDATE_OTP = ROOT + '/validate-otp';
export const POST_REFRESH_TOKEN = ROOT + '/refresh-token';

export const POST_MOSQUE = ROOT + '/find';
export const GET_MOSQUE = ROOT + '/mosque/{identifier}';
export const GET_MOSQUE_LIST = ROOT + '/mosque/list';
export const GET_MOSQUE_SEARCH = ROOT + '/mosque/search';
export const GET_MOSQUE_SEARCH_NEARBY = ROOT + '/mosque/search-nearby';
export const PATCH_MOSQUE = ROOT + '/mosque/{id}';
export const PATCH_MOSQUE_TIMES = ROOT + '/mosque/{id}/times';

export const GET_GEO_IP = 'https://freegeoip.net/json/';

const Api = {
	ROOT,
	// user
	POST_REGISTER,
	GET_USER,
	PATCH_USER,
	DELETE_USER,
	// auth
	POST_REQUEST_OTP,
	POST_VALIDATE_OTP,
	POST_REFRESH_TOKEN,
	// mosque
	POST_MOSQUE,
	GET_MOSQUE,
	GET_MOSQUE_LIST,
	GET_MOSQUE_SEARCH,
	GET_MOSQUE_SEARCH_NEARBY,
	PATCH_MOSQUE,
	PATCH_MOSQUE_TIMES
};
Object.freeze(Api);

export default Api;
