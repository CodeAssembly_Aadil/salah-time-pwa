// App Level Mutations
export const LOADER_VISIBILITY = 'LOADER_VISIBILITY';
export const APP_TITLE = 'APP_TITLE';

export const QUEUE_TOAST = 'QUEUE_TOAST';
export const DEQUEUE_TOAST = 'DEQUEUE_TOAST';
export const REMOVE_TOAST = 'REMOVE_TOAST';
export const QUEUE_SNACKBAR = 'QUEUE_SNACKBAR';
export const DEQUEUE_SNACKBAR = 'DEQUEUE_SNACKBAR';
export const REMOVE_SNACKBAR = 'REMOVE_SNACKBAR';

// User Level Mutations
export const SET_USER = 'SET_USER';
export const CLEAR_USER = 'CLEAR_USER';

export const SET_AUTH = 'SET_AUTH';
export const CLEAR_AUTH = 'CLEAR_AUTH';

export const SHOW_INTRO = 'SHOW_INTRO';
export const SHOW_GEO_NOTICE = 'SHOW_GEO_NOTICE';

export const ADD_BOOKMARK = 'ADD_BOOKMARK';
export const REMOVE_BOOKMARK = 'REMOVE_BOOKMARK';
export const SET_TIMEFORMAT = 'SET_TIMEFORMAT';
export const SET_LOCATION_ACCESS = 'SET_LOCATION_ACCESS';

export const SET_LOCATION = 'SET_LOCATION';
export const CLEAR_LOCATION = 'CLEAR_LOCATION';
