const HijriMonths = [ 'Muḥarram', 'Ṣafar', 'Rabī‘ al-awwal', 'Rabī‘ ath-thānī', 'Jumādá al-ūlá', 'Jumādá al-ākhirah', 'Rajab', 'Sha‘bān', 'Ramaḍān', 'Shawwāl', 'Dhū al-Qa‘dah', 'Dhū al-Ḥijjah' ];

export default HijriMonths;
