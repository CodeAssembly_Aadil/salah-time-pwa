export const FAJR = 'Fajr';
export const DHUHR = 'Dhuhr';
export const JUMUAH = 'Jumuah';
export const ASR = 'Asr';
export const MAGHRIB = 'Maghrib';
export const ISHA = 'Isha';
