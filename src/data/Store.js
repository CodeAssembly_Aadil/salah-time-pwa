import Vue from 'vue';
import Vuex from 'vuex';

import User from 'data/User';
import * as Mutations from 'data/MutationTypes';

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		user: User
	},

	state: {
		showLoader: false,
		title: 'Salah Times',
		toasts: [],
		snackbars: []
	},

	getters: {
		canUseDeviceLocation () {
			return Boolean(navigator.geolocation);
		}
	},

	mutations: {
		[Mutations.LOADER_VISIBILITY] (state, visible) {
			state.showLoader = visible;
		},

		[Mutations.APP_TITLE] (state, title) {
			state.title = title;
		}
	},

	actions: {
	}
});
