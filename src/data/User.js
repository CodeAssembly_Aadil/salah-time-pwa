
import * as Api from 'data/Api';
import * as Mutations from 'data/MutationTypes';

import Axios from 'axios';
import LocalForage from 'localforage';
import Moment from 'moment';

export default {
	state: {

		uuid: null,

		firstname: null,
		lastname: null,
		organization: null,
		phone_number: null,

		token: null,
		token_created_at: null,

		settings: {
			timeFormat: 24,
			hasAllowedGeoLocation: false
		},

		uiSettings: {
			showGeoNotice: true,
			showIntro: true
		},

		bookmarks: [],

		location: {
			accuracy: Number.POSITIVE_INFINITY,
			timestamp: Date.now(),
			latitude: -28.1648433,
			longitude: 23.7374648
		}
	},

	getters: {
		isAuthorized: (state) => {
			return state.token !== null && Moment.utc().isBefore(state.expires_at);
		},

		isRegisterd: (state) => {
			return state.id !== null;
		},

		userPosition (state) {
			return {
				lat: state.location.latitude,
				lng: state.location.longitude
			};
		}
	},

	mutations: {

		[Mutations.SET_USER] (state, user) {
			Object.keys(state).forEach((key) => {
				if (user.hasOwnProperty(key)) {
					state[key] = user[key];
				}
			});
		},

		[Mutations.CLEAR_USER] (state) {
			Object.keys(state).forEach((key) => {
				state[key] = null;
			});
		},

		[Mutations.SET_LOCATION] (state, location) {
			if (state.location.accuracy >= location.accuracy && state.location.timestamp <= location.timestamp) {
				state.location.accuracy = location.accuracy;
				state.location.timestamp = location.timestamp;
				state.location.longitude = location.longitude;
				state.location.latitude = location.latitude;
			}
		},

		[Mutations.CLEAR_LOCATION] (state) {
		},

		[Mutations.SET_AUTH] (state, auth) {
			state.token = auth.token;
			state.expires_at = auth.expires_at;
		},

		[Mutations.CLEAR_AUTH] (state, auth) {
			state.token = null;
			state.expires_at = null;
		},

		[Mutations.SET_TIMEFORMAT] (state, format) {
			state.settings.timeFormat = format;
		},

		[Mutations.SET_LOCATION_ACCESS] (state, permission) {
			state.settings.hasAllowedGeoLocation = permission;
		},

		[Mutations.SHOW_INTRO] (state, show) {
			state.uiSettings.showIntro = show;
		},

		[Mutations.SHOW_GEO_NOTICE] (state, show) {
			state.uiSettings.showGeoNotice = show;
		},

		[Mutations.ADD_BOOKMARK] (state, mosque) {
			const index = state.bookmarks.findIndex(
				(bookmark) => {
					return bookmark.id === mosque.id;
				}
			);

			if (index === -1) {
				state.bookmarks.push(mosque);
			}
		},

		[Mutations.REMOVE_BOOKMARK] (state, mosqueID) {
			const index = state.bookmarks.findIndex(
				(bookmark) => {
					return bookmark.id === mosqueID;
				}
			);

			if (index !== -1) {
				state.bookmarks.splice(index, 1);
			}
		}

	},

	actions: {
		saveUser ({ state }) {
			return LocalForage.setItem('user', state)
				.then(function (value) {
					return true;
					// console.info( 'User.save', 'success' );
				})
				.catch(function (error) {
					console.error('User.save', error);
					return true;
				});
		},

		fetchUser ({ state, commit }) {
			return LocalForage.getItem('user')
				.then(function (value) {
					// console.info( 'User.fetch', 'success' );
					if (value) {
						commit(Mutations.SET_USER, value);
					}
					return value;
				})
				.catch(function (error) {
					console.error('User.fetch', error);
					return error;
				});
		},
		// Set time formart
		setTimeFormat ({ dispatch, commit }, format) {
			commit(Mutations.SET_TIMEFORMAT, format);
			return dispatch('saveUser');
		},
		// Add Bookmark
		addBookmark ({ dispatch, commit }, mosque) {
			commit(Mutations.ADD_BOOKMARK, mosque);
			return dispatch('saveUser');
		},
		// Remove Bookmark
		removeBookmark ({ dispatch, commit }, mosqueID) {
			commit(Mutations.REMOVE_BOOKMARK, mosqueID);
			return dispatch('saveUser');
		},

		introVisibility ({ dispatch, commit }, show) {
			commit(Mutations.SHOW_INTRO, show);
			return dispatch('saveUser');
		},

		showGeoNotice ({ dispatch, commit }, show) {
			commit(Mutations.SHOW_GEO_NOTICE, show);
			return dispatch('saveUser');
		},
		// Remove Bookmark
		getGeoIp ({ commit, state }) {

			return Axios.get(Api.GET_GEO_IP)
				.then((result) => {

					const location = {
						accuracy: Number.POSITIVE_INFINITY,
						latitude: result.data.latitude,
						longitude: result.data.longitude,
						timestamp: Date.now()
					};

					commit(Mutations.SET_LOCATION, location);
				})
				.catch((error) => {
					console.error('getGeoIp', error);
				});
		},
		// Remove Bookmark
		getGeoLocation ({ commit, state }) {

			let promise = new Promise(function (resolve, reject) {

				const options = {
					enableHighAccuracy: true,
					timeout: 30000,
					maximumAge: 0
				};

				navigator.geolocation.getCurrentPosition(resolve, reject, options);
			});

			promise.then((position) => {

				const location = {
					accuracy: position.coords.accuracy,
					latitude: position.coords.latitude,
					longitude: position.coords.longitude,
					timestamp: position.timestamp
				};

				commit(Mutations.SET_LOCATION, location);
				commit(Mutations.SET_LOCATION_ACCESS, true);

			}).catch((error) => {
				if (error.type === 1) {
					commit(Mutations.SET_LOCATION_ACCESS, false);
				}
			});

			return promise;
		}

	}
};
