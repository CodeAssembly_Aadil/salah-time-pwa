import Vue from 'vue';
import Router from 'vue-router';

import NotFound404 from 'components/error/NotFound404';

import Index from 'components/Index';
import Mosque from 'components/mosque/Mosque';

import MosqueForm from 'components/mosque/MosqueForm';
import MosqueTimes from 'components/mosque/MosqueTimes';

import SignUp from 'components/user/SignUp';
import Activate from 'components/user/Activate';

import Profile from 'components/user/Profile';
import Bookmarks from 'components/user/Bookmarks';

import About from 'components/about/About';

Vue.use( Router );

const router = new Router( {

	mode: 'history',

	routes: [
		{
			path: '/',
			name: 'index',
			component: Index,
			meta: { title: 'Salah Time' }
		},
		{
			path: '/bookmarks',
			name: 'bookmarks',
			component: Bookmarks,
			meta: { title: 'Bookmarks' }
		},
		{
			path: '/profile',
			name: 'profile',
			component: Profile,
			meta: { title: 'Profile' }
		},
		{
			path: '/about',
			name: 'about',
			component: About,
			meta: { title: 'About' }
		},
		{
			path: '/mosque/create',
			name: 'mosque-create',
			component: MosqueForm,
			props: false

		},
		{
			path: '/mosque/:mosqueID/times',
			name: 'mosque-times',
			component: MosqueTimes,
			props: true

		},
		{
			path: '/mosque/:mosqueID/edit',
			name: 'mosque-edit',
			component: MosqueForm,
			props: true
		},
		{
			path: '/mosque/:mosqueID',
			name: 'mosque',
			component: Mosque,
			props: true

		},
		{
			path: '/signup',
			name: 'signup',
			component: SignUp,
			meta: { title: 'Sign Up' }
		},
		{
			path: '/activate',
			alias: '/login',
			name: 'activate',
			component: Activate,
			meta: { title: 'Activate' }
		},
		{
			path: '*',
			component: NotFound404,
			meta: { title: 'Resource Not Found' }
		}
	]
} );

router.beforeEach( ( to, from, next ) => {
	if ( to.meta.hasOwnProperty( 'title' ) ) {
		document.title = to.meta.title + ' - Salah Time';
	}
	next();
} );

export default router;
